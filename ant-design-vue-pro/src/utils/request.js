import axios from 'axios'
import store from '@/store'
import storage from 'store'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import Vue from 'vue'
import router from '@/router'
// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 6000 // 请求超时时间
})

// 异常拦截处理器
const errorHandler = (error) => {
  notification.error({
    message: '服务暂时不可用，请刷新页面后再试'
  })
  if (error.response) {
    const token = Vue.ls.get(ACCESS_TOKEN)
    if (!token) {
      store.dispatch('Logout').then(() => {
        setTimeout(() => {
          window.location.reload()
        }, 1500)
      })
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  const token = storage.get(ACCESS_TOKEN)
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers['authorization'] = 'Bearer ' + token
  }
  return config
}, errorHandler)

request.interceptors.response.use((response) => {
  console.log(response)
  if (response.statusText === 'OK') {
    if (response.data.code !== parseInt(process.env.VUE_APP_API_RESPONSE_SUCCESS_CODE)) {
      if (response.data.code === parseInt(process.env.VUE_APP_API_RESPONSE_EXPIRED_CODE)) {
        router.push({ path: '/user/login' })

        setTimeout(() => {
          notification.error({
            message: response.data.msg
          })
        }, 1000)
      } else {
        notification.error({
          message: response.data.msg
        })
      }
    } else {
      return response.data
    }
  } else {
    return Promise.reject(new Error('error'))
  }
}, errorHandler)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
