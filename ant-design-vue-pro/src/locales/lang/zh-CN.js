import antd from 'ant-design-vue/es/locale-provider/zh_CN'
import momentCN from 'moment/locale/zh-cn'

const components = {
  antLocale: antd,
  momentName: 'zh-cn',
  momentLocale: momentCN
}

const locale = {
  'message': '-',
  'menu.home': '主页',
  'menu.dashboard': '仪表盘',
  'menu.dashboard.analysis': '分析页',
  'menu.dashboard.monitor': '监控页',
  'menu.dashboard.workplace': '工作台',

  'layouts.usermenu.dialog.title': '消息',
  'layouts.usermenu.dialog.content': '你真的要注销吗？',

  'app.setting.pagestyle': '页面样式设置',
  'app.setting.pagestyle.light': '浅色风格',
  'app.setting.pagestyle.dark': '深色风格',
  'app.setting.pagestyle.realdark': '黑暗风格',
  'app.setting.themecolor': '主题颜色',
  'app.setting.navigationmode': '导航模式',
  'app.setting.content-width': '内容宽度',
  'app.setting.fixedheader': '固定 Header',
  'app.setting.fixedsidebar': '固定侧栏菜单',
  'app.setting.sidemenu': '侧菜单布局',
  'app.setting.topmenu': '顶部菜单布局',
  'app.setting.content-width.fixed': '固定',
  'app.setting.content-width.fluid': '流体',
  'app.setting.othersettings': '其他设置',
  'app.setting.weakmode': '弱色模式',
  'app.setting.copy': '复制设置',
  'app.setting.loading': '正在加载主题',
  'app.setting.copyinfo': '配置栏只在开发环境用于预览，生产环境不会展现，请手动修改配置文件 src/models/setting.js',
  'app.setting.production.hint': '设置面板仅在开发环境中显示，请手动修改'
}

export default {
  ...components,
  ...locale
}
