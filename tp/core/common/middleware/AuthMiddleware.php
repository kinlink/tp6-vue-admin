<?php
/**
 * 登录验证中间件
 * User: Zhong Weiwei
 * Date: 2020/7/30
 * Time: 14:57
 */

namespace app\common\middleware;

use app\common\Response;
use thans\jwt\facade\JWTAuth;
use think\Middleware;

class AuthMiddleware extends Middleware
{
    public function handle($request, \Closure $next)
    {
        try {
            JWTAuth::auth();
        } catch (\Exception $e) {
            Response::error($e->getMessage());
        }
        return $next($request);
    }

}
