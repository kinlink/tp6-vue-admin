<?php
/**
 * 后台基类控制器
 * User: Zhong Weiwei
 * Date: 2020/7/30
 * Time: 14:05
 */

namespace core\common;

use thans\jwt\facade\JWTAuth;

class Admin extends Base
{
    /**
     * 不需要验证的操作
     * @var array
     */
    protected $noAuth   =   [
        'User/login','User/loginout'
    ];

    /**
     * 初始化
     * @author: Zhong Weiwei
     * @Date: 17:00  2020/7/30
     */
    protected function initialize()
    {
        try {
//            !in_array($this->request->controller().'/'.$this->request->action(), $this->noAuth) &&  JWTAuth::auth();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
