<?php
/**
 * User: Zhong Weiwei
 * Date: 2020/7/30
 * Time: 16:13
 */

namespace core\system\subscribe;

use think\Event;
use core\system\model\Admin;

class UserSubscribe
{
    public function onUserLogin()
    {
        return Admin::login();
    }

    public function subscribe(Event $event)
    {
        $event->listen('UserLogin', [$this,'onUserLogin']);
    }
}
