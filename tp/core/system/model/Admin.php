<?php
/**
 * 管理员模型
 * User: Zhong Weiwei
 * Date: 2020/7/31
 * Time: 14:50
 */

namespace core\system\model;
use think\Model;
use core\common\Response;
use \thans\jwt\facade\JWTAuth;
class Admin extends Model
{
    /**
     * 管理员登录
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author: Zhong Weiwei
     * @Date: 16:03  2020/7/31
     */
    public static function login()
    {
        $username   =   input('username','','trim');
        $password   =   input('password','','trim');
        !$username  &&  Response::error('用户名不能为空！');
        !$password  &&  Response::error('登录密码不能为空！');

        $map    =   [
            'username'  =>  $username,
            'password'  =>  md5(ADMIN_LOGIN_KEY.$password)
        ];
        $userInfo   =   self::where($map)->field("id as uid,'ADMIN' as utype,username,avatar,nickname,email")->find();
        !$userInfo  &&  Response::error('用户名或密码错误！');
        $userInfo['token'] = JWTAuth::builder(['uid' => $userInfo['uid']]);
        JWTAuth::setToken($userInfo['token']);
        global $member;
        $member = (object)$userInfo;

        /*更新登录信息*/
        self::update_login_info();

        /*创建登录会话*/
        Response::session_commit();

        Response::success('登录成功！',$member->token);
    }

    /**
     * 更新登录信息
     * @author: Zhong Weiwei
     * @Date: 16:15  2020/7/31
     */
    public static function update_login_info()
    {
        global $member;
        if (!$member->uid)return;
        self::where('id','=',$member->uid)->update([
            'logintime' =>  time(),
            'loginip'   =>  request()->ip()
        ]);
    }

}
