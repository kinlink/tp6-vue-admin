<?php
declare (strict_types=1);

namespace core\system\event;

class LoadModuleRoutes
{
    /**
     * @author: Zhong Weiwei
     * @Date: 16:05  2020/9/4
     */
    public function handle(): void
    {
        include "../route.php";
    }
}
