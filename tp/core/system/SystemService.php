<?php
namespace core\system;
use think\Service;

class SystemService extends Service
{
    protected $events   =   [
        'bind'      => [],

        'listen'    => [
            'AppInit'  => [],
            'HttpRun'  => [],
            'HttpEnd'  => [],
            'LogLevel' => [],
            'LogWrite' => [],
            'RouteLoaded' => [],
        ],

        'subscribe' => [
            \core\system\subscribe\UserSubscribe::class
        ],
    ];
    /**
     *
     * @time 2019年11月29日
     * @return void
     */
    public function boot()
    {
    }

    /**
     * register
     *
     * @author JaguarJack
     * @email njphper@gmail.com
     * @time 2020/1/30
     * @return void
     */
    public function register()
    {
        dump(111);die;
        $this->registerEvents();
        //加载路由
        $this->registerRoutes(function () {
            include "route.php";
        });
    }


    /**
     * 注册监听者
     *
     * @time 2019年12月12日
     * @return void
     */
    protected function registerEvents(): void
    {
        $this->app->event->listenEvents($this->events);
    }


}
