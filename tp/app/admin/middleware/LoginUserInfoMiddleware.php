<?php
/**
 * 获取当前登录用户信息中间件
 * User: Zhong Weiwei
 * Date: 2020/7/31
 * Time: 13:48
 */

namespace app\admin\middleware;


use think\Middleware;

class LoginUserInfoMiddleware extends Middleware
{
    public function handle($request, \Closure $next)
    {

    }
}
