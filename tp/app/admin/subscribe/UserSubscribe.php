<?php
/**
 * User: Zhong Weiwei
 * Date: 2020/7/30
 * Time: 16:13
 */

namespace app\admin\subscribe;

use think\Event;
use app\admin\model\Admin;

class UserSubscribe
{
    public function onUserLogin()
    {
        return Admin::login();
    }

    public function subscribe(Event $event)
    {
        $event->listen('UserLogin', [$this,'onUserLogin']);
    }
}
