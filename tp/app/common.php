<?php
// 应用公共文件


if (!function_exists('default_anonymous_user')) {
    /**
     * @return stdClass
     * @author: Zhong Weiwei
     * @Date: 13:39  2020/7/31
     */
    function default_anonymous_user()
    {
        $user = new \stdClass();
        $user->username = "Guest";
        $user->utype = "GUEST";
        $user->uid = 0;    //会员ID
        $user->gname = '游客';
        return $user;
    }
}




