<?php
/**
 * 请求响应返回
 * User: Zhong Weiwei
 * Date: 2020/7/31
 * Time: 15:55
 */

namespace app\common;

use think\exception\HttpResponseException;
use think\facade\Db;

class Response
{
    protected static $errorMsg =   [
        ERROR       =>  '未知错误'
    ];
    /**
     * 失败返回
     * @param string $msg
     * @param int $code
     * @param array $data
     * @param array $other
     * @author: Zhong Weiwei
     * @Date: 15:28  2020/7/31
     */
    public static function error($msg = '', $code = ERROR, $data = [], $other = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg  ?   $msg    :   self::$errorMsg[$code],
            'data' => $data,
        ];

        foreach ($other as $key=>$item){
            $result[$key]   =   $item;
        }
        throw new HttpResponseException(json($result));
    }

    /**
     * 成功返回
     * @param string $msg
     * @param array $data
     * @param array $other
     * @author: Zhong Weiwei
     * @Date: 15:29  2020/7/31
     */
    public static function success($msg = '',  $data = [], $other = [])
    {
        $result = [
            'code' => SUCCESS,
            'msg'  => $msg,
            'data' => $data,
        ];

        foreach ($other as $key=>$item){
            $result[$key]   =   $item;
        }
        throw new HttpResponseException(json($result));
    }

    /**
     * 创建登录会话
     * @throws \think\db\exception\DbException
     * @author: Zhong Weiwei
     * @Date: 16:24  2020/7/31
     */
    public static function session_commit()
    {
        global $member;

        /*回收当前账号其他登录会话*/
        Db::name('session')->where([
            'uid'   =>  $member->uid,
            'utype' =>  $member->utype
        ])->delete();

        /*创建新的登录会话*/
        Db::name('session')->insert([
            'utype'         =>  $member->utype,
            'uid'           =>  $member->uid,
            'session_id'    =>  md5($member->token),
            'session_time'  =>  date('Y-m-d H:i:s'),
            'session_data'  =>  $member->token,
            'ip'            =>  request()->ip()
        ]);
    }
}
