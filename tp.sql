/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : tp

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2020-08-07 15:57:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dd_admin
-- ----------------------------
DROP TABLE IF EXISTS `dd_admin`;
CREATE TABLE `dd_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员表';

-- ----------------------------
-- Records of dd_admin
-- ----------------------------
INSERT INTO `dd_admin` VALUES ('1', 'admin', 'Admin', 'd0f60f069696e7ef863a4267bfc5a8b4', '417de8', '/assets/img/avatar.png', 'admin@admin.com', '0', '1596770883', '127.0.0.1', '1492186163', '1595230586', '76eba42e-3bc7-41d3-80b4-428c2988ba55', 'normal');

-- ----------------------------
-- Table structure for dd_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `dd_auth_group`;
CREATE TABLE `dd_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户组中文名',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `rules` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of dd_auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for dd_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `dd_auth_group_access`;
CREATE TABLE `dd_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

-- ----------------------------
-- Records of dd_auth_group_access
-- ----------------------------

-- ----------------------------
-- Table structure for dd_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `dd_auth_rule`;
CREATE TABLE `dd_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of dd_auth_rule
-- ----------------------------

-- ----------------------------
-- Table structure for dd_session
-- ----------------------------
DROP TABLE IF EXISTS `dd_session`;
CREATE TABLE `dd_session` (
  `utype` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用户类型',
  `uid` int(10) NOT NULL COMMENT '用户ID',
  `session_id` varchar(120) COLLATE utf8_unicode_ci NOT NULL COMMENT 'session_id或token',
  `session_time` datetime NOT NULL COMMENT '最后会话时间',
  `session_data` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '当前登录IP',
  PRIMARY KEY (`utype`),
  UNIQUE KEY `uid` (`uid`,`session_id`),
  KEY `utype` (`utype`,`uid`,`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='当前在线登录会话';

-- ----------------------------
-- Records of dd_session
-- ----------------------------
INSERT INTO `dd_session` VALUES ('ADMIN', '1', 'd99e1f5cac9f54f166d6d97cb514bf4e', '2020-08-07 11:28:03', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjEsImF1ZCI6IiIsImV4cCI6MTU5Njc3MDk0MywiaWF0IjoxNTk2NzcwODgzLCJpc3MiOiIiLCJqdGkiOiJlZThiMmIxODJkZjhjMmNkZWRiMTdmYTNmNmU3MDM3MCIsIm5iZiI6MTU5Njc3MDg4Mywic3ViIjoiIn0.a_uR61AnCAbIkeGMbtUSNmkCOViOPSrpAjl-9B9TJKc', '127.0.0.1');
